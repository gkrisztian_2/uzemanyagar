﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Uzemanyag
{
    enum DateType
    {
        Short,
        Long
    }
    class Datum // Mivel itt volt több idő, saját osztályt készítettem a Dátum kezelésére is.
    {
        public int Ev { get; set; }
        public int Honap { get; set; }
        public int Nap { get; set; }

        private string[] _honapok = { "január", "február", "március", "április", "május", "június", "július", "augusztus", "szeptember", "október", "november", "december" };

        public Datum(string d)
        {
            string[] s = d.Split('.');

            this.Ev = int.Parse(s[0]);
            this.Honap = int.Parse(s[1]);
            this.Nap = int.Parse(s[2]);
        }

        public string Kiir(DateType d)
        {
            return d == DateType.Short ? $"{this.Ev}.{FormatDigits(this.Honap)}.{FormatDigits(this.Nap)}" : $"{this.Ev}. {_honapok[this.Honap - 1]}. {FormatDigits(this.Nap)}"; 
            // Short: 0000.00.00 - Long: 0000.honap.00
        }
        string FormatDigits(int n)
        {
            return n > 9 ? "0" + n : n.ToString();
        }
        public string Kiir()
        {
            return $"{this.Ev}.{this.Honap}.{this.Nap}";
            //0000.0.0
        }

        public bool isSzokoEv()
        {
            return this.Ev % 4 == 0;
        }
    }
    class Adat
    {
        public Datum datum { get; private set; }
        public double benzin { get; private set; }
        public double gazolaj { get; private set; }

        public Adat(string str)
        {
            string[] s = str.Split(';');

            this.datum = new Datum(s[0]);
            this.benzin = double.Parse(s[1]);
            this.gazolaj = double.Parse(s[2]);

        }

        public double Kulonbseg
        {
            get { return Math.Abs(benzin - gazolaj); } 
        }

        public int ElteltNapok(Adat e)
        {
            int[] napokSzama = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

            if (this.datum.isSzokoEv())
                napokSzama[1] = 29;

            if (this.datum.Honap == e.datum.Honap)
            {
                return this.datum.Nap - e.datum.Nap;
            }

            return napokSzama[e.datum.Honap - 1] - e.datum.Nap + this.datum.Nap;
        }
    }
    class Uzemanyag
    {
        static List<Adat> l = new List<Adat>();

        static void Main(string[] args)
        {
            Read("uzemanyag.txt", Encoding.Default);

            Console.WriteLine("3. feladat: Változások száma: " + l.Count);

            double legkisebbKul = l.Min(x => x.Kulonbseg);
            Console.WriteLine("4. feladat: A legkisebb különbség: " + legkisebbKul);

            int legkisebbKulCount = l.Count(x => x.Kulonbseg == legkisebbKul);
            Console.WriteLine("5. feladat: A legkisebb különbség előfordulása: " + legkisebbKulCount);

            bool voltSzokValt = l.Count(x => x.datum.isSzokoEv() && x.datum.Honap == 2 && x.datum.Nap == 24) > 0;
            Console.WriteLine("6. feladat: " + (voltSzokValt ? "Volt változás szökőnapon!" : "Nem volt változás szökőnapon!"));

            Write("euro.txt");

            int evSz = 0;
            do
            {
                Console.Write("8. feladat: Kérem adja meg az évszámot [2011..2016]: ");
                evSz = int.Parse(Console.ReadLine());
            } while (evSz < 2011 || evSz > 2016);

            int leghosszabb = 0, temp = 0;
            for (int i = 1; i < l.Count; i++)
            {
                temp = l[i].ElteltNapok(l[i - 1]);
                if (l[i].datum.Ev == evSz && l[i-1].datum.Ev == evSz)
                {
                    if(temp > leghosszabb)
                        leghosszabb = temp;
                }
                //Console.WriteLine(temp);
            }
            Console.WriteLine("10. feladat: {0} évben a leghosszabb időszak {1} nap volt.", evSz, leghosszabb);
            
            Console.ReadKey();
        }
        static void Write(string f)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(f))
                {
                    const double EUR = 307.7;

                    foreach (Adat item in l)
                    {
                        sw.WriteLine("{0};{1};{2}", item.datum.Kiir(), (item.benzin/EUR).ToString("f2"), (item.gazolaj / EUR).ToString("f2"));
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        static void Read(string f, Encoding enc)
        {
            try
            {
                using (StreamReader sr = new StreamReader(f, enc))
                {
                    while (!sr.EndOfStream)
                    {
                        l.Add(new Adat(sr.ReadLine()));
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
